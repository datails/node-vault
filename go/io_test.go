package utils

import (
	"io/ioutil"
	"os"
	"testing"
)

func TestWebAssemblyWriteAndRead(t *testing.T) {
	tempFile, err := ioutil.TempFile("", "testfile.txt")
	if err != nil {
		t.Fatalf("Error creating temporary file: %v", err)
	}
	defer os.Remove(tempFile.Name())

	t.Run("WebAssemblyWrite", func(t *testing.T) {
		data := "This is a test data."
		result, err := WebAssemblyWrite(tempFile.Name(), data)

		if err != nil {
			t.Errorf("WebAssemblyWrite() error = %v", err)
		}

		if !result {
			t.Errorf("WebAssemblyWrite() failed to write data to file")
		}

		fileContent, _ := ioutil.ReadFile(tempFile.Name())
		if string(fileContent) != data {
			t.Errorf("WebAssemblyWrite() file content = %v, expected data = %v", string(fileContent), data)
		}
	})

	t.Run("WebAssemblyRead", func(t *testing.T) {
		data, err := WebAssemblyRead(tempFile.Name())

		if err != nil {
			t.Errorf("WebAssemblyRead() error = %v", err)
		}

		expectedData := "This is a test data."
		if data != expectedData {
			t.Errorf("WebAssemblyRead() data = %v, expectedData = %v", data, expectedData)
		}
	})

	t.Run("WebAssemblyReadNonExistentFile", func(t *testing.T) {
		_, err := WebAssemblyRead("nonexistent.txt")

		if err == nil {
			t.Errorf("WebAssemblyRead() expected an error for non-existent file, but got nil")
		}
	})
}
