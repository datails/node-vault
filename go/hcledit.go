package utils

import (
	"errors"
	"strings"

	"github.com/hashicorp/hcl/v2/hclwrite"
	"go.mercari.io/hcledit"
)

func UpdateAttr(filePath string, key string, value interface{}) (bool, error) {
	editor, err := hcledit.ReadFile(filePath)

	if err != nil {
		return false, errors.New("Couldn't read file at: " + filePath)
	}

	err = editor.Create(key, value)

	if err != nil {
		return false, errors.New("Error creating attribute: " + err.Error())
	}

	err = editor.OverWriteFile()
	if err != nil {
		return false, errors.New("Error writing changes to file: " + err.Error())
	}

	return true, nil
}

func CreateAttr(filePath string, key string, value interface{}) (bool, error) {
	editor, err := hcledit.ReadFile(filePath)

	if err != nil {
		return false, errors.New("Couldn't read file at: " + filePath)
	}

	err = editor.Create(key, value)

	if err != nil {
		return false, errors.New("Error creating attribute: " + err.Error())
	}

	err = editor.OverWriteFile()
	if err != nil {
		return false, errors.New("Error writing changes to file: " + err.Error())
	}

	return true, nil
}

func DelAttr(filePath string, key string) (bool, error) {
	editor, err := hcledit.ReadFile(filePath)

	if err != nil {
		return false, errors.New("Couldn't read file at: " + filePath)
	}

	err = editor.Delete(key)

	if err != nil {
		return false, errors.New("Error deleting attribute: " + err.Error())
	}

	err = editor.OverWriteFile()
	if err != nil {
		return false, errors.New("Error writing changes to file: " + err.Error())
	}

	return true, nil
}

func GetKeys(filePath string, key string) ([]string, error) {
	editor, err := hcledit.ReadFile(filePath)
	if err != nil {
		return nil, errors.New("Couldn't read file at: " + filePath)
	}

	valueMap, err := editor.Read(key)
	if err != nil {
		return nil, errors.New("Error occurred during reading: " + key)
	}

	if len(valueMap) == 0 {
		return nil, nil
	}

	var keys []string
	for k := range valueMap {
		keys = append(keys, k)
	}

	return keys, nil
}

func GetAttr(filePath string, key string) (interface{}, error) {
	editor, err := hcledit.ReadFile(filePath)
	if err != nil {
		return nil, errors.New("Couldn't read file at: " + filePath)
	}

	valueMap, err := editor.Read(key)
	if err != nil {
		return nil, errors.New("Error occurred during reading: " + key)
	}

	if len(valueMap) == 0 {
		return nil, nil
	}

	if len(valueMap) > 1 {
		var values []interface{}

		for _, v := range valueMap {
			values = append(values, v)
		}

		return values, nil
	}

	for _, v := range valueMap {
		// Return the value as is, without type assertion
		return v, nil
	}

	return nil, nil
}

func RenameResourceBlock(filePath, key, newKey string) (bool, error) {
	editor, err := hcledit.ReadFile(filePath)

	if err != nil {
		return false, errors.New("Couldn't read file at: " + filePath)
	}

	keyParts := strings.Split(key, ".")
	newKeyParts := strings.Split(newKey, ".")

	if len(keyParts) != 2 || len(newKeyParts) != 2 {
		return false, errors.New("Invalid key format. Expected format 'type.name'")
	}

	currentType := keyParts[0]
	currentName := keyParts[1]
	newType := newKeyParts[0]

	err = editor.CustomEdit(func(b *hclwrite.Body) error {
		for _, block := range b.Blocks() {
			if block.Type() == currentType && len(block.Labels()) > 0 && block.Labels()[0] == currentName {
				// Update the block type
				block.SetType(newType)
				block.SetLabels(newKeyParts[1:])
			}
		}
		return nil
	})

	if err != nil {
		return false, errors.New("Error renaming resource block: " + err.Error())
	}

	err = editor.OverWriteFile()
	if err != nil {
		return false, errors.New("Error writing changes to file: " + err.Error())
	}

	return true, nil
}

func DelBlock(filePath, key string) (bool, error) {
	editor, err := hcledit.ReadFile(filePath)

	if err != nil {
		return false, errors.New("Couldn't read file at " + filePath)
	}

	err = editor.CustomEdit(func(b *hclwrite.Body) error {
		// Split the blockType into its individual parts
		blockTypeParts := strings.Split(key, ".")

		// Find the block to delete
		var blockToDelete *hclwrite.Block

		for _, block := range b.Blocks() {
			// Check if the block type matches the first part of blockType
			if block.Type() == blockTypeParts[0] {
				// Initialize a flag to check if all parts of blockType match
				matchAllParts := true

				// Start the loop from the second part (skip the block type)
				for i, part := range blockTypeParts[1:] {
					// Check if the current label matches the current part of blockType
					if len(block.Labels()) > i && block.Labels()[i] != part {
						// If there's a mismatch, set the flag to false and break
						matchAllParts = false
						break
					}
				}

				// If all parts match, set the block to delete and break the loop
				if matchAllParts {
					blockToDelete = block
					break
				}
			}
		}

		// If the block to delete is found, remove it
		if blockToDelete != nil {
			b.RemoveBlock(blockToDelete)
		}

		return nil
	})

	if err != nil {
		return false, errors.New("Couldn't delete block at " + filePath)
	}

	err = editor.OverWriteFile()
	if err != nil {
		return false, errors.New("Error writing changes to file: " + err.Error())
	}

	return true, nil
}
