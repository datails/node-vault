package utils

import (
	"bytes"
	"encoding/json"

	"github.com/hashicorp/hcl"
	"github.com/hashicorp/hcl/hcl/printer"
)

func ToJSON(str string) (string, error) {
	var v interface{}
	err := hcl.Unmarshal([]byte(str), &v)

	if err != nil {
		return "", err
	}

	data, err := json.MarshalIndent(v, "", "    ")
	if err != nil {
		return "", err
	}

	return string(data), nil
}

func ToHCL(str string) (string, error) {
	v, err := hcl.Parse(str)

	if err != nil {
		return "", err
	}

	var buf bytes.Buffer

	err = printer.Fprint(&buf, v)
	if err != nil {
		return "", err
	}

	return buf.String(), nil
}
