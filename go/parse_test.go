package utils

import (
	"regexp"
	"strings"
	"testing"
)

func normalizeContent(content string) string {
	content = strings.ReplaceAll(content, "\r\n", "\n")
	content = strings.TrimSpace(content)
	content = regexp.MustCompile(`\s+`).ReplaceAllString(content, " ")
	return content
}

func TestToJSON(t *testing.T) {
	tests := []struct {
		name     string
		input    string
		expected string
		wantErr  bool
	}{
		{
			name:     "Valid JSON input",
			input:    `{"key": "value"}`,
			expected: "{\n    \"key\": \"value\"\n}\n",
			wantErr:  false,
		},
		{
			name:     "Invalid JSON input",
			input:    `invalid_json`,
			expected: "", // Expected empty string for error case
			wantErr:  true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			result, err := ToJSON(tt.input)
			normalizedResult := normalizeContent(result)
			normalizedExpected := normalizeContent(tt.expected)

			if (err != nil) != tt.wantErr {
				t.Errorf("ToJSON() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if normalizedResult != normalizedExpected {
				t.Errorf("ToJSON() got = %v, want %v", normalizedResult, normalizedExpected)
			}
		})
	}
}

func TestToHCL(t *testing.T) {
	tests := []struct {
		name     string
		input    string
		expected string
		wantErr  bool
	}{
		{
			name:     "Valid HCL input",
			input:    `key = "value"`,
			expected: "key = \"value\"\n",
			wantErr:  false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			result, err := ToHCL(tt.input)
			normalizedResult := normalizeContent(result)
			normalizedExpected := normalizeContent(tt.expected)

			if (err != nil) != tt.wantErr {
				t.Errorf("ToHCL() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if normalizedResult != normalizedExpected {
				t.Errorf("ToHCL() got = %v, want %v", normalizedResult, normalizedExpected)
			}
		})
	}
}
