package utils

import (
	"fmt"
	"io/ioutil"
	"os"
)

func WebAssemblyWrite(filePath string, data string) (bool, error) {
	file, err := os.Create(filePath)
	if err != nil {
		fmt.Println("Error creating a file in the WebAssembly sandbox:", err)
		return false, err
	}
	defer file.Close()

	_, err = file.WriteString(data)
	if err != nil {
		fmt.Println("Error writing to file in WebAssembly sandbox:", err)
		return false, err
	}

	return true, nil
}

func WebAssemblyRead(filePath string) (string, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return "", err
	}
	defer file.Close()

	data, err := ioutil.ReadAll(file)
	if err != nil {
		return "", err
	}

	return string(data), nil
}
