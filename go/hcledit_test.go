package utils

import (
	"io/ioutil"
	"os"
	"reflect"
	"sort"
	"strings"
	"testing"
)

func normalizeHCLContent(content string) string {
	// Remove all whitespaces including spaces, tabs, and newlines
	content = strings.Join(strings.Fields(content), "")
	return content
}

func compareStringSlices(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}

func TestGetKeys(t *testing.T) {
	tempFile, err := ioutil.TempFile("", "testfile.tf")
	if err != nil {
		t.Fatalf("Error creating temporary file: %v", err)
	}
	defer os.Remove(tempFile.Name())

	initialContent := `
    resource "aws_s3_bucket" "my_bucket" {
        foo = "bar"
    }

    resource "aws_s3_bucket" "my_bucket_1" {
        foo = "bar"
    }`
	err = ioutil.WriteFile(tempFile.Name(), []byte(initialContent), 0644)
	if err != nil {
		t.Fatalf("Error writing to temporary file: %v", err)
	}

	expectedKeys := []string{
		"resource.aws_s3_bucket.my_bucket.foo",
		"resource.aws_s3_bucket.my_bucket_1.foo",
	}

	keys, err := GetKeys(tempFile.Name(), "resource.aws_s3_bucket.*.foo")
	if err != nil {
		t.Errorf("GetKeys() error = %v", err)
		return
	}

	sort.Strings(keys)
	sort.Strings(expectedKeys)

	if !reflect.DeepEqual(keys, expectedKeys) {
		t.Errorf("GetKeys() keys = %v, expectedKeys %v", keys, expectedKeys)
	}
}

func TestUpdateAttr(t *testing.T) {
	tempFile, err := ioutil.TempFile("", "testfile.tf")
	if err != nil {
		t.Fatalf("Error creating temporary file: %v", err)
	}
	defer os.Remove(tempFile.Name())

	initialContent := `
	resource "example" {
		name = "initial"
	}`
	err = ioutil.WriteFile(tempFile.Name(), []byte(initialContent), 0644)
	if err != nil {
		t.Fatalf("Error writing to temporary file: %v", err)
	}

	tests := []struct {
		name           string
		filePath       string
		key            string
		value          interface{}
		expectedHCL    string
		expectedErr    bool
		expectedErrMsg string
	}{
		{
			name:     "Update existing attribute",
			filePath: tempFile.Name(),
			key:      "resource.example.name",
			value:    "updated",
			expectedHCL: `
			resource "example" {
				name = "updated"
			}`,
			expectedErr:    false,
			expectedErrMsg: "",
		},
		{
			name:     "Update existing attribute",
			filePath: tempFile.Name(),
			key:      "resource.example.name",
			value:    6000,
			expectedHCL: `
			resource "example" {
				name = 6000
			}`,
			expectedErr:    false,
			expectedErrMsg: "",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := UpdateAttr(tt.filePath, tt.key, tt.value)

			if (err != nil) != tt.expectedErr {
				t.Errorf("UpdateAttr() error = %v, expectedErr %v", err, tt.expectedErr)
				return
			}

			if tt.expectedErr && err.Error() != tt.expectedErrMsg {
				t.Errorf("UpdateAttr() error message = %v, expectedErrMsg %v", err.Error(), tt.expectedErrMsg)
				return
			}

			if !tt.expectedErr {
				fileContent, _ := ioutil.ReadFile(tt.filePath)
				expectedHCLNormalized := normalizeHCLContent(tt.expectedHCL)
				fileContentNormalized := normalizeHCLContent(string(fileContent))

				if fileContentNormalized != expectedHCLNormalized {
					t.Errorf("UpdateAttr() file content = %v, expectedHCL %v", fileContentNormalized, expectedHCLNormalized)
				}
			}
		})
	}
}

func TestCreateAttr(t *testing.T) {
	tempFile, err := ioutil.TempFile("", "testfile.tf")
	if err != nil {
		t.Fatalf("Error creating temporary file: %v", err)
	}
	defer os.Remove(tempFile.Name())

	initialContent := `
	resource "example" {
		name = "initial"
	}`
	err = ioutil.WriteFile(tempFile.Name(), []byte(initialContent), 0644)
	if err != nil {
		t.Fatalf("Error writing to temporary file: %v", err)
	}

	tests := []struct {
		name           string
		filePath       string
		key            string
		value          interface{}
		expectedHCL    string
		expectedErr    bool
		expectedErrMsg string
	}{
		{
			name:     "Create new attribute",
			filePath: tempFile.Name(),
			key:      "resource.example.new_attr",
			value:    "value",
			expectedHCL: `
			resource "example" {
				name     = "initial"
				new_attr = "value"
			}`,
			expectedErr:    false,
			expectedErrMsg: "",
		},
		{
			name:     "Create new attribute",
			filePath: tempFile.Name(),
			key:      "resource.example.new_attr",
			value:    6000,
			expectedHCL: `
			resource "example" {
				name     = "initial"
				new_attr = 6000
			}`,
			expectedErr:    false,
			expectedErrMsg: "",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := CreateAttr(tt.filePath, tt.key, tt.value)

			if (err != nil) != tt.expectedErr {
				t.Errorf("CreateAttr() error = %v, expectedErr %v", err, tt.expectedErr)
				return
			}

			if tt.expectedErr && err.Error() != tt.expectedErrMsg {
				t.Errorf("CreateAttr() error message = %v, expectedErrMsg %v", err.Error(), tt.expectedErrMsg)
				return
			}

			if !tt.expectedErr {
				fileContent, _ := ioutil.ReadFile(tt.filePath)
				expectedHCLNormalized := normalizeHCLContent(tt.expectedHCL)
				fileContentNormalized := normalizeHCLContent(string(fileContent))

				if fileContentNormalized != expectedHCLNormalized {
					t.Errorf("CreateAttr() file content = %v, expectedHCL %v", fileContentNormalized, expectedHCLNormalized)
				}
			}
		})
	}
}

func TestDelAttr(t *testing.T) {
	tempFile, err := ioutil.TempFile("", "testfile.tf")
	if err != nil {
		t.Fatalf("Error creating temporary file: %v", err)
	}
	defer os.Remove(tempFile.Name())

	initialContent := `
	resource "example" {
		name     = "initial"
		new_attr = "value"
	}`
	err = ioutil.WriteFile(tempFile.Name(), []byte(initialContent), 0644)
	if err != nil {
		t.Fatalf("Error writing to temporary file: %v", err)
	}

	tests := []struct {
		name           string
		filePath       string
		key            string
		expectedHCL    string
		expectedErr    bool
		expectedErrMsg string
	}{
		{
			name:     "Delete existing attribute",
			filePath: tempFile.Name(),
			key:      "resource.example.new_attr",
			expectedHCL: `
			resource "example" {
				name = "initial"
			}`,
			expectedErr:    false,
			expectedErrMsg: "",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := DelAttr(tt.filePath, tt.key)

			if (err != nil) != tt.expectedErr {
				t.Errorf("DelAttr() error = %v, expectedErr %v", err, tt.expectedErr)
				return
			}

			if tt.expectedErr && err.Error() != tt.expectedErrMsg {
				t.Errorf("DelAttr() error message = %v, expectedErrMsg %v", err.Error(), tt.expectedErrMsg)
				return
			}

			if !tt.expectedErr {
				fileContent, _ := ioutil.ReadFile(tt.filePath)
				expectedHCLNormalized := normalizeHCLContent(tt.expectedHCL)
				fileContentNormalized := normalizeHCLContent(string(fileContent))

				if fileContentNormalized != expectedHCLNormalized {
					t.Errorf("DelAttr() file content = %v, expectedHCL %v", fileContentNormalized, expectedHCLNormalized)
				}
			}
		})
	}
}

func TestDelBlock(t *testing.T) {
	tempFile, err := ioutil.TempFile("", "testfile.tf")
	if err != nil {
		t.Fatalf("Error creating temporary file: %v", err)
	}
	defer os.Remove(tempFile.Name())

	initialContent := `
	resource "example" {
		name     = "initial"
		new_attr = "value"
	}
	`
	err = ioutil.WriteFile(tempFile.Name(), []byte(initialContent), 0644)
	if err != nil {
		t.Fatalf("Error writing to temporary file: %v", err)
	}

	tests := []struct {
		name           string
		filePath       string
		key            string
		expectedHCL    string
		expectedErr    bool
		expectedErrMsg string
	}{
		{
			name:     "Delete existing block",
			filePath: tempFile.Name(),
			key:      `resource.example`,
			expectedHCL: `
			`,
			expectedErr:    false,
			expectedErrMsg: "",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := DelBlock(tt.filePath, tt.key)

			if (err != nil) != tt.expectedErr {
				t.Errorf("DelBlock() error = %v, expectedErr %v", err, tt.expectedErr)
				return
			}

			if tt.expectedErr && err.Error() != tt.expectedErrMsg {
				t.Errorf("DelBlock() error message = %v, expectedErrMsg %v", err.Error(), tt.expectedErrMsg)
				return
			}

			if !tt.expectedErr {
				fileContent, _ := ioutil.ReadFile(tt.filePath)
				expectedHCLNormalized := normalizeHCLContent(tt.expectedHCL)
				fileContentNormalized := normalizeHCLContent(string(fileContent))

				if fileContentNormalized != expectedHCLNormalized {
					t.Errorf("DelBlock() file content = %v, expectedHCL %v", fileContentNormalized, expectedHCLNormalized)
				}
			}
		})
	}
}

func TestGetAttr(t *testing.T) {
	tempFile, err := ioutil.TempFile("", "testfile.tf")
	if err != nil {
		t.Fatalf("Error creating temporary file: %v", err)
	}
	defer os.Remove(tempFile.Name())

	initialContent := `
	resource "example" "cd" {
		name     = "initial"
		new_attr = "value"
		another_attr = 5000
	}
	
	resource "example" "ab" {
		name     = "initial"
		new_attr = "value"
	}
	`
	err = ioutil.WriteFile(tempFile.Name(), []byte(initialContent), 0644)
	if err != nil {
		t.Fatalf("Error writing to temporary file: %v", err)
	}

	tests := []struct {
		name           string
		filePath       string
		key            string
		expectedValue  interface{}
		expectedErr    bool
		expectedErrMsg string
	}{
		{
			name:           "Get string attribute",
			filePath:       tempFile.Name(),
			key:            "resource.example.ab.new_attr",
			expectedValue:  "value",
			expectedErr:    false,
			expectedErrMsg: "",
		},
		{
			name:           "Get string attributes",
			filePath:       tempFile.Name(),
			key:            "resource.example.*.new_attr",
			expectedValue:  []interface{}{"value", "value"},
			expectedErr:    false,
			expectedErrMsg: "",
		},
		{
			name:           "Get number attribute",
			filePath:       tempFile.Name(),
			key:            "resource.example.cd.another_attr",
			expectedValue:  5000,
			expectedErr:    false,
			expectedErrMsg: "",
		},
		{
			name:           "Get non-existing attribute",
			filePath:       tempFile.Name(),
			key:            "resource.example.non_existent_attr",
			expectedValue:  nil,
			expectedErr:    false,
			expectedErrMsg: "",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			value, err := GetAttr(tt.filePath, tt.key)

			if (err != nil) != tt.expectedErr {
				t.Errorf("GetAttr() error = %v, expectedErr %v", err, tt.expectedErr)
				return
			}

			if tt.expectedErr && err.Error() != tt.expectedErrMsg {
				t.Errorf("GetAttr() error message = %v, expectedErrMsg %v", err.Error(), tt.expectedErrMsg)
				return
			}

			if valueSlice, ok := value.([]string); ok {
				if expectedSlice, ok := tt.expectedValue.([]string); ok {
					if !compareStringSlices(valueSlice, expectedSlice) {
						t.Errorf("GetAttr() value = %v, expectedValue %v", value, tt.expectedValue)
					}
				} else {
					t.Errorf("Expected value is not a slice")
				}
			} else {
				if !reflect.DeepEqual(value, tt.expectedValue) {
					t.Errorf("GetAttr() value = %v, expectedValue %v", value, tt.expectedValue)
				}
			}
		})
	}
}

func TestRenameResourceBlock(t *testing.T) {
	tempFile, err := ioutil.TempFile("", "testfile.tf")
	if err != nil {
		t.Fatalf("Error creating temporary file: %v", err)
	}
	defer os.Remove(tempFile.Name())

	initialContent := `
	resource "example" {
		name     = "initial"
		new_attr = "value"
	}
	`
	err = ioutil.WriteFile(tempFile.Name(), []byte(initialContent), 0644)
	if err != nil {
		t.Fatalf("Error writing to temporary file: %v", err)
	}

	tests := []struct {
		name           string
		filePath       string
		key            string
		newKey         string
		expectedHCL    string
		expectedErr    bool
		expectedErrMsg string
	}{
		{
			name:     "Rename existing block",
			filePath: tempFile.Name(),
			key:      `resource.example`,
			newKey:   `resource.example2`,
			expectedHCL: `
			resource "example2" {
				name     = "initial"
				new_attr = "value"
			}`,
			expectedErr:    false,
			expectedErrMsg: "",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := RenameResourceBlock(tt.filePath, tt.key, tt.newKey)

			if (err != nil) != tt.expectedErr {
				t.Errorf("RenameResourceBlock() error = %v, expectedErr %v", err, tt.expectedErr)
				return
			}

			if tt.expectedErr && err.Error() != tt.expectedErrMsg {
				t.Errorf("RenameResourceBlock() error message = %v, expectedErrMsg %v", err.Error(), tt.expectedErrMsg)
				return
			}

			if !tt.expectedErr {
				fileContent, _ := ioutil.ReadFile(tt.filePath)
				expectedHCLNormalized := normalizeHCLContent(tt.expectedHCL)
				fileContentNormalized := normalizeHCLContent(string(fileContent))

				if fileContentNormalized != expectedHCLNormalized {
					t.Errorf("RenameResourceBlock() file content = %v, expectedHCL %v", fileContentNormalized, expectedHCLNormalized)
				}
			}
		})
	}
}
