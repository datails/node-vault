module node-hcl-edit

go 1.20

require go.mercari.io/hcledit v0.0.9

require (
	github.com/agext/levenshtein v1.2.3 // indirect
	github.com/apparentlymart/go-textseg/v13 v13.0.0 // indirect
	github.com/google/go-cmp v0.5.9 // indirect
	github.com/hashicorp/hcl v1.0.0
	github.com/hashicorp/hcl/v2 v2.17.0 // indirect
	github.com/kr/pretty v0.3.1 // indirect
	github.com/mitchellh/go-wordwrap v1.0.1 // indirect
	github.com/zclconf/go-cty v1.13.2 // indirect
	golang.org/x/text v0.11.0 // indirect
)
