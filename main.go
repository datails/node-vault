package main

import (
	"fmt"
	utils "node-hcl-edit/go"
	"reflect"
	"syscall/js"
)

type fn func(this js.Value, args []js.Value) (js.Value, error)

var (
	jsErr     js.Value = js.Global().Get("Error")
	jsPromise js.Value = js.Global().Get("Promise")
)

func AsyncFunc(innerFunc fn) js.Func {
	return js.FuncOf(func(this js.Value, args []js.Value) any {
		handler := js.FuncOf(func(_ js.Value, promFn []js.Value) any {
			resolve, reject := promFn[0], promFn[1]

			go func() {
				defer func() {
					if r := recover(); r != nil {
						reject.Invoke(jsErr.New(fmt.Sprint("panic:", r)))
					}
				}()

				res, err := innerFunc(this, args)
				if err != nil {
					reject.Invoke(jsErr.New(err.Error()))
				} else {
					resolve.Invoke(res)
				}
			}()

			return nil
		})

		return jsPromise.New(handler)
	})
}

// utility fn, to first add a file to the
// webassembly sandbox, in order to read files from the webassembly disk.
func WebAssemblyRead(this js.Value, args []js.Value) (js.Value, error) {
	fp := args[0].String()

	returnVal, err := utils.WebAssemblyRead(fp)

	if err != nil {
		return js.ValueOf(returnVal), err
	}

	return js.ValueOf(returnVal), nil
}

// utility fn, to first add a file to the
// webassembly sandbox, in order to read files from the webassembly disk.
func WebAssemblyWrite(this js.Value, args []js.Value) (js.Value, error) {
	fp := args[0].String()
	data := args[1].String()

	returnVal, err := utils.WebAssemblyWrite(fp, data)

	if err != nil {
		return js.ValueOf(returnVal), err
	}

	return js.ValueOf(returnVal), nil
}

func ToJSON(this js.Value, args []js.Value) (js.Value, error) {
	input := args[0].String()
	returnVal, err := utils.ToJSON(input)

	if err != nil {
		return js.ValueOf(nil), err
	}

	return js.ValueOf(returnVal), nil
}

func ToHCL(this js.Value, args []js.Value) (js.Value, error) {
	input := args[0].String()
	returnVal, err := utils.ToHCL(input)

	if err != nil {
		return js.ValueOf(nil), err
	}

	return js.ValueOf(returnVal), nil
}

func UpdateAttr(this js.Value, args []js.Value) (js.Value, error) {
	fp := args[0].String()
	k := args[1].String()
	v := args[2].String()

	returnVal, err := utils.UpdateAttr(fp, k, v)

	if err != nil {
		return js.ValueOf(returnVal), err
	}

	return js.ValueOf(returnVal), nil
}

func CreateAttr(this js.Value, args []js.Value) (js.Value, error) {
	fp := args[0].String()
	k := args[1].String()
	v := args[2].String()

	returnVal, err := utils.CreateAttr(fp, k, v)

	if err != nil {
		return js.ValueOf(returnVal), err
	}

	return js.ValueOf(returnVal), nil
}

func RenameResourceBlock(this js.Value, args []js.Value) (js.Value, error) {
	fp := args[0].String()
	k := args[1].String()
	newK := args[2].String()

	returnVal, err := utils.RenameResourceBlock(fp, k, newK)

	if err != nil {
		return js.ValueOf(returnVal), err
	}

	return js.ValueOf(returnVal), nil
}

func DelAttr(this js.Value, args []js.Value) (js.Value, error) {
	fp := args[0].String()
	k := args[1].String()

	returnVal, err := utils.DelAttr(fp, k)

	if err != nil {
		return js.ValueOf(returnVal), err
	}

	return js.ValueOf(returnVal), nil
}

func GetKeys(this js.Value, args []js.Value) (js.Value, error) {
	fp := args[0].String()
	k := args[1].String()

	returnVal, err := utils.GetKeys(fp, k)

	interfaceSlice := make([]interface{}, len(returnVal))
	for i, v := range returnVal {
		interfaceSlice[i] = v
	}

	if err != nil {
		return js.ValueOf(nil), err
	}

	if returnVal == nil {
		return js.ValueOf(nil), nil
	}

	return js.ValueOf(interfaceSlice), nil
}

func DelBlock(this js.Value, args []js.Value) (js.Value, error) {
	fp := args[0].String()
	k := args[1].String()

	returnVal, err := utils.DelBlock(fp, k)

	if err != nil {
		return js.ValueOf(returnVal), err
	}

	return js.ValueOf(returnVal), nil
}

func GetAttr(this js.Value, args []js.Value) (js.Value, error) {
	fp := args[0].String()
	k := args[1].String()

	returnVal, err := utils.GetAttr(fp, k)

	if returnVal == nil {
		return js.ValueOf(nil), nil
	}

	if err != nil {
		return js.ValueOf(nil), err
	}

	if returnVal == "" {
		return js.ValueOf(nil), nil
	}

	if reflect.TypeOf(returnVal).Kind() == reflect.Slice {
		s := reflect.ValueOf(returnVal)
		interfaceSlice := make([]interface{}, s.Len())
		for i := 0; i < s.Len(); i++ {
			interfaceSlice[i] = s.Index(i).Interface()
		}

		return js.ValueOf(interfaceSlice), nil
	}

	return js.ValueOf(returnVal), nil
}

func main() {
	// wrap the fn in an async fn, because else we block the js event loop
	// see: https://pkg.go.dev/syscall/js#FuncOf
	js.Global().Set("readFromSandbox", AsyncFunc(WebAssemblyRead))
	js.Global().Set("writeToSandbox", AsyncFunc(WebAssemblyWrite))
	js.Global().Set("delAttr", AsyncFunc(DelAttr))
	js.Global().Set("delBlock", AsyncFunc(DelBlock))
	js.Global().Set("renameResourceBlock", AsyncFunc(RenameResourceBlock))
	js.Global().Set("createAttr", AsyncFunc(UpdateAttr))
	js.Global().Set("updateAttr", AsyncFunc(CreateAttr))
	js.Global().Set("getAttr", AsyncFunc(GetAttr))
	js.Global().Set("getKeys", AsyncFunc(GetKeys))
	js.Global().Set("toJSON", AsyncFunc(ToJSON))
	js.Global().Set("toHCL", AsyncFunc(ToHCL))
	select {}
}
