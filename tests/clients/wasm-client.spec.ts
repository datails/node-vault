import { describe, it } from "node:test";
import assert from "node:assert";
import {
  isObject,
  isFunction,
  isWasmContextLoaded,
} from "../../src/clients/wasm-client/utils";

describe("isObject", () => {
  it("should return true for objects", () => {
    assert.ok(isObject({}));
    assert.ok(isObject({ foo: "bar" }));
  });

  it("should return false for non-objects", () => {
    assert.strictEqual(isObject([]), false);
    assert.strictEqual(isObject("string"), false);
    assert.strictEqual(isObject(42), false);
    assert.strictEqual(isObject(null), false);
    assert.strictEqual(isObject(undefined), false);
    assert.strictEqual(isObject(true), false);
    assert.strictEqual(isObject(false), false);
    assert.strictEqual(
      isObject(() => {}),
      false,
    );
  });
});

describe("isFunction", () => {
  it("should return true for functions", () => {
    assert.ok(isFunction(() => {}));
    assert.ok(isFunction(function () {}));
    assert.ok(isFunction(function named() {}));
  });

  it("should return false for non-functions", () => {
    assert.strictEqual(isFunction({}), false);
    assert.strictEqual(isFunction([]), false);
    assert.strictEqual(isFunction("string"), false);
    assert.strictEqual(isFunction(42), false);
    assert.strictEqual(isFunction(null), false);
    assert.strictEqual(isFunction(undefined), false);
    assert.strictEqual(isFunction(true), false);
    assert.strictEqual(isFunction(false), false);
  });
});

describe("isWasmContextLoaded", () => {
  it("should return false when the context is not loaded", () => {
    const ctx = {};
    assert.strictEqual(isWasmContextLoaded(ctx), false);
  });

  it("should return false when the context is partially loaded", () => {
    const ctx = {
      updateAttr: () => {},
    };
    assert.strictEqual(isWasmContextLoaded(ctx), false);
  });

  it("should return true when the context is fully loaded", () => {
    const ctx = {
      writeToSandbox: () => {},
      readFromSandbox: () => {},
      renameResourceBlock: () => {},
      delBlock: () => {},
      toJSON: () => {},
      toHCL: () => {},
      updateAttr: () => {},
      createAttr: () => {},
      delAttr: () => {},
      getAttr: () => {},
      getKeys: () => {},
    };
    assert.ok(isWasmContextLoaded(ctx));
  });
});
