import assert from "node:assert";
import { describe, it } from "node:test";
import {
  createThrowErr,
  updateAttrErr,
  createAttrErr,
  delAttrErr,
  wasmErr,
} from "../../src/errors";
import { HclEditError } from "../../src/errors/errors";

describe("createThrowErr", () => {
  it("should throw an error with the provided message and cause", () => {
    const errMsg = "Test error message";
    const causeErr = new Error("Cause error message");

    const fn = createThrowErr(HclEditError, errMsg);

    try {
      fn(causeErr);
    } catch (error) {
      assert.strictEqual(error.message, errMsg);
      assert.strictEqual(error.cause, causeErr);
    }
  });
});

describe("updateAttrErr", () => {
  it('should throw an error with the "Update attribute failed." message', () => {
    const causeErr = new Error("Cause error message");

    try {
      updateAttrErr(causeErr);
    } catch (error) {
      assert.strictEqual(error.message, "Update attribute failed.");
      assert.strictEqual(error.cause, causeErr);
    }
  });
});

describe("createAttrErr", () => {
  it('should throw an error with the "Create attribute failed." message', () => {
    const causeErr = new Error("Cause error message");

    try {
      createAttrErr(causeErr);
    } catch (error) {
      assert.strictEqual(error.message, "Create attribute failed.");
      assert.strictEqual(error.cause, causeErr);
    }
  });
});

describe("delAttrErr", () => {
  it('should throw an error with the "Delete attribute failed." message', () => {
    const causeErr = new Error("Cause error message");

    try {
      delAttrErr(causeErr);
    } catch (error) {
      assert.strictEqual(error.message, "Delete attribute failed.");
      assert.strictEqual(error.cause, causeErr);
    }
  });
});

describe("wasmErr", () => {
  it('should throw an error with the "Failed to parse main.wasm." message', () => {
    const causeErr = new Error("Cause error message");

    try {
      wasmErr(causeErr);
    } catch (error) {
      assert.strictEqual(error.message, "Failed to parse main.wasm.");
      assert.strictEqual(error.cause, causeErr);
    }
  });
});
