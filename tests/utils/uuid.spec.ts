import assert from "node:assert";
import { describe, it } from "node:test";
import { v4 as generateUUID } from "../../src/utils/uuid";

describe("generateUUID", () => {
  it("should generate a valid UUID", () => {
    const uuid = generateUUID();

    const uuidRegex =
      /^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i;
    assert.ok(uuid.match(uuidRegex));
  });

  it("should generate unique UUIDs", () => {
    const uuid1 = generateUUID();
    const uuid2 = generateUUID();

    assert.notStrictEqual(uuid1, uuid2);
  });
});
