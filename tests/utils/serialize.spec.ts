import assert from "node:assert";
import { describe, it } from "node:test";
import {
  addQuotesAroundVariables,
  removeQuotesAroundVariables,
} from "../../src/utils/serialize";

describe("addQuotesAroundVariables", () => {
  it("should add quotes around variables", () => {
    const input = `
      module "example" {
        source = "github.com/example"
        variable1 = local.variable1
        variable2 = var.variable2
      }
    `;
    const expectedOutput = `
      module "example" {
        source = "github.com/example"
        variable1 = "local.variable1"
        variable2 = "var.variable2"
      }
    `;
    const result = addQuotesAroundVariables(input);
    assert.strictEqual(result, expectedOutput);
  });

  it("should handle nested object variables", () => {
    const input = `
    module "example" {
      source = "github.com/example"
      nested_variable = data.foo.bar[0].foobar
    `;
    const expectedOutput = `
    module "example" {
      source = "github.com/example"
      nested_variable = "data.foo.bar[0].foobar"
    `;
    const result = addQuotesAroundVariables(input);
    assert.strictEqual(result, expectedOutput);
  });

  it("should convert null values to a special string", () => {
    const input = `
    resource "example" {
      attribute = null
    }
  `;
    const expectedOutput = `
    resource "example" {
      attribute = "__NULL__"
    }
  `;
    const result = addQuotesAroundVariables(input);
    assert.strictEqual(result, expectedOutput);
  });

  it("should not modify input without variables", () => {
    const input = `
      module "example" {
        source = "github.com/example"
        key = "value"
      }
    `;
    const result = addQuotesAroundVariables(input);
    assert.strictEqual(result, input);
  });
});

describe("removeQuotesAroundVariables", () => {
  it("should remove quotes around variables", () => {
    const input = `
      module "example" {
        source = "github.com/example"
        variable1 = "local.variable1"
        variable2 = "var.variable2"
      }
    `;
    const expectedOutput = `
      module "example" {
        source = "github.com/example"
        variable1 = local.variable1
        variable2 = var.variable2
      }
    `;
    const result = removeQuotesAroundVariables(input);
    assert.strictEqual(result, expectedOutput);
  });

  it("should convert the special string back to null values", () => {
    const input = `
    resource "example" {
      attribute = "__NULL__"
    }
  `;
    const expectedOutput = `
    resource "example" {
      attribute = null
    }
  `;
    const result = removeQuotesAroundVariables(input);
    assert.strictEqual(result, expectedOutput);
  });

  it("should not modify input without quotes around variables", () => {
    const input = `
      module "example" {
        source = "github.com/example"
        key = "value"
      }
    `;
    const result = removeQuotesAroundVariables(input);
    assert.strictEqual(result, input);
  });

  it("should handle variables with nested arrays", () => {
    const input = `
      module "example" {
        source = "github.com/example"
        nested_variable = "data.foo.bar[0].foobar[1].baz"
      `;
    const expectedOutput = `
      module "example" {
        source = "github.com/example"
        nested_variable = data.foo.bar[0].foobar[1].baz
      `;
    const result = removeQuotesAroundVariables(input);
    assert.strictEqual(result, expectedOutput);
  });
});
