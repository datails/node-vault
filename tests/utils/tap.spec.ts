import assert from "node:assert";
import { describe, it } from "node:test";
import { asyncTap } from "../../src/utils/tap";

const asyncFunction = async () => {
  await new Promise((resolve) => setTimeout(resolve, 100));
};

describe("asyncTap", () => {
  it("should return the same argument after tapping", async () => {
    const tappedAsyncFunction = asyncTap(asyncFunction);
    const result = await tappedAsyncFunction(42);
    assert.strictEqual(result, 42);
  });
});
