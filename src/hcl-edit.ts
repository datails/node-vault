import { createProviders } from "./providers";
import { createDataSources } from "./datasources";
import type { HclOptions } from "./models";
import { createClients } from "./clients";
import { createParsers } from "./parsers";

export type HclEdit = ReturnType<typeof createHclEdit>;

export const createHclEdit = async (options: HclOptions = {}) => {
  const clients = await createClients();
  const dataSources = createDataSources(clients, options);
  const providers = createProviders(dataSources);
  const parsers = createParsers(providers);

  return parsers;
};
