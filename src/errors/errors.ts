export class HclEditError extends Error {
  constructor(message: string, options?: ErrorOptions) {
    super(message, options);
  }
}

export class BinaryNotFoundException extends HclEditError {
  constructor(message: string, options?: ErrorOptions) {
    super(message, options);
  }
}

export class ParseException extends HclEditError {
  constructor(message: string, options?: ErrorOptions) {
    super(message, options);
  }
}

export class UpdateAttrException extends HclEditError {
  constructor(message: string, options?: ErrorOptions) {
    super(message, options);
  }
}

export class CreateAttrException extends HclEditError {
  constructor(message: string, options?: ErrorOptions) {
    super(message, options);
  }
}

export class DelAttrException extends HclEditError {
  constructor(message: string, options?: ErrorOptions) {
    super(message, options);
  }
}

export class DelBlockException extends HclEditError {
  constructor(message: string, options?: ErrorOptions) {
    super(message, options);
  }
}

export class RenameBlockException extends HclEditError {
  constructor(message: string, options?: ErrorOptions) {
    super(message, options);
  }
}

export class GetAttrException extends HclEditError {
  constructor(message: string, options?: ErrorOptions) {
    super(message, options);
  }
}

export class GetKeysException extends HclEditError {
  constructor(message: string, options?: ErrorOptions) {
    super(message, options);
  }
}

export class ToJSONException extends HclEditError {
  constructor(message: string, options?: ErrorOptions) {
    super(message, options);
  }
}

export class ToHCLException extends HclEditError {
  constructor(message: string, options?: ErrorOptions) {
    super(message, options);
  }
}
