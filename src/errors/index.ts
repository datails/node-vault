import {
  BinaryNotFoundException,
  CreateAttrException,
  DelAttrException,
  DelBlockException,
  GetAttrException,
  GetKeysException,
  HclEditError,
  ParseException,
  RenameBlockException,
  ToHCLException,
  ToJSONException,
  UpdateAttrException,
} from "./errors";

export const createThrowErr =
  (Err: typeof HclEditError, msg: string) => (err: unknown) => {
    throw new Err(msg, { cause: err });
  };

export const updateAttrErr = createThrowErr(
  UpdateAttrException,
  "Update attribute failed.",
);
export const createAttrErr = createThrowErr(
  CreateAttrException,
  "Create attribute failed.",
);
export const delAttrErr = createThrowErr(
  DelAttrException,
  "Delete attribute failed.",
);
export const delBlockErr = createThrowErr(
  DelBlockException,
  "Delete block failed.",
);
export const renameBlockErr = createThrowErr(
  RenameBlockException,
  "Rename block failed.",
);
export const getAttrErr = createThrowErr(
  GetAttrException,
  "Get attribute failed.",
);
export const getKeysErr = createThrowErr(GetKeysException, "Get keys failed.");

export const wasmErr = createThrowErr(
  ParseException,
  "Failed to parse main.wasm.",
);

export const binaryNotFoundErr = createThrowErr(
  BinaryNotFoundException,
  "Binary not found.",
);

export const toJSONErr = createThrowErr(
  ToJSONException,
  "Failed to convert to JSON.",
);

export const toHCLErr = createThrowErr(
  ToHCLException,
  "Failed to convert to HCL.",
);
