import type { Providers } from "../models";

export type Parsers = ReturnType<typeof createParsers>;

export const createParsers = (providers: Providers) => ({
  ...providers.hclParseProvider,
  ...providers.hclProvider,
});
