export interface GlobalThis extends Global {
  updateAttr: <T>(filePath: string, key: string, value: T) => Promise<boolean>;
  createAttr: <T>(filePath: string, key: string, value: T) => Promise<boolean>;
  delAttr: (filePath: string, key: string) => Promise<boolean>;
  getAttr: <T>(filePath: string, key: string) => Promise<T | null>;
  delBlock: (filePath: string, key: string) => Promise<boolean>;
  renameResourceBlock: (
    filePath: string,
    key: string,
    newKey: string,
  ) => Promise<boolean>;
  getKeys: (filePath: string, key: string) => Promise<string[] | null>;
  readFromSandbox: (filePath: string) => Promise<string>;
  writeToSandbox: (filePath: string, content: string) => Promise<string>;
  toJSON: (str: string) => Promise<string>;
  toHCL: (str: string) => Promise<string>;
}
