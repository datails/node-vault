import "./wasm_exec";

import { join } from "node:path";
import { readFile } from "node:fs/promises";
import { existsSync } from "node:fs";

import { wasmErr } from "../../../errors";
import type { GlobalThis } from "../types";
import { BinaryNotFoundException } from "../../../errors/errors";

const WASM_FUNCTIONS = [
  "writeToSandbox",
  "readFromSandbox",
  "renameResourceBlock",
  "delAttr",
  "delBlock",
  "getAttr",
  "getKeys",
  "createAttr",
  "updateAttr",
  "toJSON",
  "toHCL",
] as const;

export const isObject = (obj: unknown): obj is Record<string, unknown> =>
  typeof obj === "object" && obj !== null && !Array.isArray(obj);

export const isFunction = (fn: unknown): fn is Function =>
  typeof fn === "function";

export const isWasmContextLoaded = (
  wasmContext: unknown,
): wasmContext is GlobalThis =>
  WASM_FUNCTIONS.every(
    (str) => isObject(wasmContext) && isFunction(wasmContext[str]),
  );

export const loadWasm = async (pathToWasm: string) => {
  if (!existsSync(pathToWasm)) {
    throw new BinaryNotFoundException(`File not found: ${pathToWasm}`);
  }

  const go = new Go();

  const { instance } = await WebAssembly.instantiate(
    new Uint8Array(await readFile(pathToWasm)),
    go.importObject,
  );

  // we do not await here, because in go we use the select statement
  go.run(instance);
};

export const loadWasmContext = async (retries = 0): Promise<GlobalThis> => {
  const wasmContext = globalThis;

  if (retries >= 3) {
    return wasmErr(new Error("Failed to load wasm context"));
  }

  if (isWasmContextLoaded(wasmContext)) {
    return wasmContext;
  }

  return loadWasm(join(__dirname, "..", "..", "..", "main.wasm")) // main.wasm is the compiled Go code to the root
    .then(() => loadWasmContext(retries + 1))
    .catch(wasmErr);
};

export type WasmContext = ReturnType<typeof createWasmContext>;

export const createWasmContext = (ctx: GlobalThis) => ({
  updateAttr: ctx.updateAttr,
  createAttr: ctx.createAttr,
  delAttr: ctx.delAttr,
  getAttr: ctx.getAttr,
  getKeys: ctx.getKeys,
  delBlock: ctx.delBlock,
  renameResourceBlock: ctx.renameResourceBlock,
  readFromSandbox: ctx.readFromSandbox,
  writeToSandbox: ctx.writeToSandbox,
  toJSON: ctx.toJSON,
  toHCL: ctx.toHCL,
});
