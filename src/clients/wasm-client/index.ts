import { createWasmContext, loadWasmContext } from "./utils";

export type WasmClient = Awaited<ReturnType<typeof createWasmClient>>;

export const createWasmClient = async () => {
  const ctx = await loadWasmContext();
  return createWasmContext(ctx);
};
