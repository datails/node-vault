import { createWasmClient } from "./wasm-client";

export type Clients = Awaited<ReturnType<typeof createClients>>;

export const createClients = async () => ({
  wasmClient: await createWasmClient(),
});
