export const asyncTap =
  <T>(fn: Function) =>
  async (arg: T) => {
    await fn(arg);
    return arg;
  };
