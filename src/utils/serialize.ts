export const addQuotesAroundVariables = (hclConfig: string) => {
  return hclConfig.replace(
    /(\b(local|var|data)\.[a-zA-Z_][a-zA-Z0-9_.\[\]]*\b)|null/g,
    (match) => (match === "null" ? '"__NULL__"' : `"${match}"`),
  );
};

export const removeQuotesAroundVariables = (hclConfig: string) => {
  return hclConfig.replace(
    /"((local|var|data)\.[a-zA-Z_][a-zA-Z0-9_.\[\]]*)"|"__NULL__"/g,
    (_, variable) => (variable ? variable : "null"),
  );
};
