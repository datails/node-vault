export const safeJSONParse = (json: string) => {
  try {
    return JSON.parse(json);
  } catch (e) {
    return json;
  }
};

export const safeJSONStringify = (json: any) => {
  try {
    return JSON.stringify(json);
  } catch (e) {
    return json;
  }
};
