import type { HclOptions } from "../models";
import type { Clients } from "../clients";
import { createSandBoxDataSource } from "./wasm-sandbox";

export type { UseSandBoxFn } from "./wasm-sandbox";
export type { WasmClient } from "../clients/wasm-client";

export type DataSources = ReturnType<typeof createDataSources>;

export const createDataSources = (clients: Clients, opts: HclOptions = {}) => {
  return {
    wasmClient: clients.wasmClient,
    wasmSandboxes: createSandBoxDataSource(clients.wasmClient, opts),
  };
};
