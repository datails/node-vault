export type SandboxFn<U, Y> = (
  filePath: string,
  key: string,
  value?: U,
) => Promise<Y>;
