import { rm, mkdtemp, readFile, writeFile } from "node:fs/promises";
import { join } from "node:path";
import { v4 } from "../../../../utils/uuid";
import type { HclOptions } from "../../../../models";

export type TempFile = Awaited<ReturnType<typeof createTempFile>>;

export const removeFile = (tmpDir: string) =>
  rm(tmpDir, { recursive: true, force: true });

export const createFileIfNotExists = (filePath: string) =>
  readFile(filePath).catch(() => writeFile(filePath, ""));

export const createTempFile = async (data: string, options: HclOptions) => {
  const tmpDir = options?.tempDir
    ? await mkdtemp(options.tempDir)
    : await mkdtemp(v4());

  const tmpFileName = v4();
  const tmpFilePath = join(tmpDir, tmpFileName);

  await createFileIfNotExists(tmpFilePath);
  await writeFile(tmpFilePath, data);

  return {
    tmpDir,
    tmpFileName,
    tmpFilePath,
  };
};
