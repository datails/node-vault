import assert from "node:assert";
import { describe, it } from "node:test";
import {
  existsSync,
  readFileSync,
  rmdirSync,
  unlinkSync,
  writeFileSync,
} from "node:fs";
import { createFileIfNotExists, createTempFile } from "../create-temp-file";

describe("createFileIfNotExists", () => {
  it("should create a file if it does not exist", async () => {
    const filePath = "nonexistent-file.txt";

    await createFileIfNotExists(filePath);

    assert.ok(existsSync(filePath));

    unlinkSync(filePath);
  });

  it("should not throw an error if the file already exists", async () => {
    const filePath = "existing-file.txt";
    writeFileSync(filePath, "Test content");

    await createFileIfNotExists(filePath);

    unlinkSync(filePath);
  });
});

describe("createTempFile", () => {
  it("should create a temporary file with the provided data", async () => {
    const testData = "Test data";

    const { tmpDir, tmpFilePath } = await createTempFile(testData, {
      tempDir: "foo",
    });

    assert.ok(existsSync(tmpFilePath));
    assert.ok(existsSync(tmpDir));

    // Read the content of the temporary file and compare it with the test data
    const fileContent = readFileSync(tmpFilePath, "utf8");
    assert.strictEqual(fileContent, testData);

    unlinkSync(tmpFilePath);
    rmdirSync(tmpDir);
  });
});
