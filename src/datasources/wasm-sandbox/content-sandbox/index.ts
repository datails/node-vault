import type { HclOptions } from "../../../models";
import { createThrowErr } from "../../../errors";
import { createTempFile, removeFile } from "./utils/create-temp-file";
import { asyncTap } from "../../../utils/tap";
import type { ContentSandbox } from "./sandbox";
import type { SandboxFn } from "../types";
import { createDefaultOpts } from "../options";

// this high order function is used to enable webassembly to read and write to the filesystem
// webassembly by design can't read or write to the filesystem,
// so we need to use the sandbox to read and write to the filesystem
export const createUseContentSandbox =
  (sb: ContentSandbox, opts: HclOptions = {}) =>
  <U, Y>(
    fn: SandboxFn<U, Y>,
    errFn: ReturnType<typeof createThrowErr>,
    sandboxOpts = createDefaultOpts(),
  ) =>
  async <T extends string, K extends string, V extends U | undefined>(
    ...args: [T, K, V?]
  ) => {
    const [content, key, value] = args;

    const tmpFile = await createTempFile(content, opts);

    return sb
      .toWebassembly(tmpFile.tmpFilePath, content)
      .then(() => fn(tmpFile.tmpFilePath, key, value))
      .then((resp: Y): Promise<Y | string> => {
        if (sandboxOpts.shouldSyncWebassembly) {
          return sb.fromWebassembly(tmpFile.tmpFilePath);
        }

        return Promise.resolve(resp);
      })
      .then(asyncTap(() => removeFile(tmpFile.tmpDir)))
      .catch(async (err: unknown) => {
        await removeFile(tmpFile.tmpDir);
        return errFn(err);
      });
  };
