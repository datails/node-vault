import type { WasmClient } from "../../../models";

export type ContentSandbox = ReturnType<typeof createContentSandbox>;

export const createReadFromSandBox = (wasm: WasmClient) => (filePath: string) =>
  wasm.readFromSandbox(filePath);

export const createWriteToWebassembly =
  (wasm: WasmClient) => (filePath: string, content: string) =>
    wasm.writeToSandbox(filePath, content);

export const createContentSandbox = (wasm: WasmClient) => ({
  fromWebassembly: createReadFromSandBox(wasm),
  toWebassembly: createWriteToWebassembly(wasm),
});
