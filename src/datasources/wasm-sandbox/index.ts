import type { HclOptions } from "../../models";
import type { WasmClient } from "../../clients/wasm-client";
import { createUseContentSandbox } from "./content-sandbox";
import { createContentSandbox } from "./content-sandbox/sandbox";
import { createUseFsSandbox } from "./fs-sandbox";
import { createFsSandBox } from "./fs-sandbox/sandbox";

export type SandBoxDataSource = ReturnType<typeof createSandBoxDataSource>;

export type UseSandBoxFn =
  | SandBoxDataSource["useContentSandbox"]
  | SandBoxDataSource["useFileSystemSandbox"];

export const createSandBoxDataSource = (
  wasm: WasmClient,
  opts: HclOptions = {},
) => {
  const contentSandbox = createContentSandbox(wasm);
  const fsSandbox = createFsSandBox(wasm);

  return {
    useContentSandbox: createUseContentSandbox(contentSandbox, opts),
    useFileSystemSandbox: createUseFsSandbox(fsSandbox),
  };
};
