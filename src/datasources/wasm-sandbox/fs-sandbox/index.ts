import { createThrowErr } from "../../../errors";
import { createDefaultOpts } from "../options";
import { SandboxFn } from "../types";
import type { FsSandbox } from "./sandbox";

export const createUseFsSandbox =
  (sb: FsSandbox) =>
  <U, Y>(
    fn: SandboxFn<U, Y>,
    err: ReturnType<typeof createThrowErr>,
    sandboxOpts = createDefaultOpts(),
  ) =>
  async <T extends string, K extends string, V extends U | undefined>(
    ...args: [T, K, V?]
  ) => {
    const [filePath, key, value] = args;

    return sb
      .toWebassembly(filePath)
      .then(() => fn(filePath, key, value))
      .then(async (resp) => {
        if (sandboxOpts.shouldSyncWebassembly) {
          await sb.toFileSystem(filePath);
          return resp;
        }

        return Promise.resolve(resp);
      })
      .catch(err);
  };
