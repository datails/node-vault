import { readFile, writeFile } from "node:fs/promises";
import type { WasmClient } from "../../../models";

export const createToFilesystem = (wasm: WasmClient) => (filePath: string) =>
  wasm
    .readFromSandbox(filePath)
    .then((content) => writeFile(filePath, content));

export const createToWebassembly = (wasm: WasmClient) => (filePath: string) =>
  readFile(filePath).then((content) =>
    wasm.writeToSandbox(filePath, content.toString("utf-8")),
  );

export type FsSandbox = ReturnType<typeof createFsSandBox>;

export const createFsSandBox = (wasm: WasmClient) => ({
  toFileSystem: createToFilesystem(wasm),
  toWebassembly: createToWebassembly(wasm),
});
