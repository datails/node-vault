import {
  createDeserializeOpts,
  createSerializeOpts,
} from "./providers/hcl-parse-provider/utils";
import { createHclEdit } from "./hcl-edit";
import type { HclOptions } from "./models";

export {
  addQuotesAroundVariables,
  removeQuotesAroundVariables,
} from "./utils/serialize";

export type {
  DeserializeOptions,
  SerializeOptions,
  WasmClient,
  FileParser,
  ContentParser,
  HclOptions,
  HclEdit,
} from "./models";

export * from "./errors/errors";

export { createHclEdit } from "./hcl-edit";

export type HclContentParser = ReturnType<typeof createHclContentParser>;
export type HclFileParser = ReturnType<typeof createHclFileParser>;

export const createHclContentParser = async (options?: HclOptions) => {
  const { contentParser } = await createHclEdit(options);
  return contentParser;
};

export const createHclFileParser = async (options?: HclOptions) => {
  const { fileParser } = await createHclEdit(options);
  return fileParser;
};

export const toJSON = async <T = Record<string, unknown>>(
  str: string,
  opts = createSerializeOpts(),
) => {
  const { toJSON } = await createHclEdit();
  return toJSON<T>(str, opts);
};

export const toHCL = async <T = Record<string, unknown>>(
  obj: T,
  opts = createDeserializeOpts(),
) => {
  const { toHCL } = await createHclEdit();
  return toHCL<T>(obj, opts);
};
