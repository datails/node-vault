import {
  addQuotesAroundVariables,
  removeQuotesAroundVariables,
} from "../../../utils/serialize";
import type { DeserializeOptions, SerializeOptions } from "../types";

export const createSerializeOpts = (
  opts: Partial<SerializeOptions> = {},
): SerializeOptions => ({
  serializeVariables: true,
  serialize: addQuotesAroundVariables,
  ...opts,
});

export const createDeserializeOpts = (
  opts: Partial<DeserializeOptions> = {},
): DeserializeOptions => ({
  deserializeVariables: true,
  deserialize: removeQuotesAroundVariables,
  ...opts,
});
