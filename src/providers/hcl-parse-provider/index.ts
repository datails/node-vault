import type { DataSources } from "../../models";
import { createToHCL, createToJSON } from "./provider";

export type HclParseProvider = ReturnType<typeof createHclParseProvider>;

export const createHclParseProvider = (dataSources: DataSources) => ({
  toJSON: createToJSON(dataSources.wasmClient),
  toHCL: createToHCL(dataSources.wasmClient),
});
