import { safeJSONParse, safeJSONStringify } from "../../utils/parse";
import { toHCLErr, toJSONErr } from "../../errors";
import type { Clients } from "../../models";
import { createDeserializeOpts, createSerializeOpts } from "./utils";

export const createToJSON =
  (wasmClient: Clients["wasmClient"]) =>
  async <T = Record<string, any>>(
    str: string,
    opts = createSerializeOpts(),
  ): Promise<T> =>
    wasmClient
      .toJSON(opts.serializeVariables ? opts.serialize(str) : str)
      .then(safeJSONParse)
      .catch(toJSONErr);

export const createToHCL =
  (wasmClient: Clients["wasmClient"]) =>
  async <T = Record<string, any>>(
    obj: T,
    opts = createDeserializeOpts(),
  ): Promise<string> =>
    wasmClient
      .toHCL(safeJSONStringify(obj))
      .then((resp) =>
        opts.deserializeVariables ? opts.deserialize(resp) : resp,
      )
      .catch(toHCLErr);
