export interface SerializeOptions {
  serializeVariables: boolean;
  serialize: (hclConfig: string) => string;
}

export interface DeserializeOptions {
  deserializeVariables: boolean;
  deserialize: (hclConfig: string) => string;
}
