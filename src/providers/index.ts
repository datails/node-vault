import type { DataSources } from "../models";
import { createHclParseProvider } from "./hcl-parse-provider";
import { createHclProviders } from "./hcl-provider";

export type { FileParser, ContentParser } from "./hcl-provider";
export type Providers = ReturnType<typeof createProviders>;

export const createProviders = (dataSources: DataSources) => ({
  hclProvider: createHclProviders(dataSources),
  hclParseProvider: createHclParseProvider(dataSources),
});
