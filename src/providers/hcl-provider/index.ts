import type { DataSources } from "../../models";
import { createHclProvider } from "./provider";
import type { ContentParser, FileParser } from "./types";

export type { ContentParser, FileParser } from "./types";
export type HclProviders = ReturnType<typeof createHclProviders>;

export const createHclProviders = (dataSources: DataSources) => ({
  contentParser: createHclProvider(
    dataSources,
    dataSources.wasmSandboxes.useContentSandbox,
  ) as ContentParser,
  fileParser: createHclProvider(
    dataSources,
    dataSources.wasmSandboxes.useFileSystemSandbox,
  ) as FileParser,
});
