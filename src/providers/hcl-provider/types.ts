export type ContentParser = {
  updateAttr: <T>(content: string, key: string, value: T) => Promise<string>;
  createAttr: <T>(content: string, key: string, value: T) => Promise<string>;
  delAttr: (content: string, key: string) => Promise<string>;
  getAttr: <T>(content: string, key: string) => Promise<T | null>;
  delBlock: (content: string, key: string) => Promise<string>;
  renameResourceBlock: (
    content: string,
    key: string,
    newKey: string,
  ) => Promise<string>;
  getKeys: (content: string, key: string) => Promise<string[] | null>;
};

export type FileParser = {
  updateAttr: <T>(filePath: string, key: string, value: T) => Promise<boolean>;
  createAttr: <T>(filePath: string, key: string, value: T) => Promise<boolean>;
  delAttr: (filePath: string, key: string) => Promise<boolean>;
  getAttr: <T>(filePath: string, key: string) => Promise<T | null>;
  delBlock: (filePath: string, key: string) => Promise<boolean>;
  renameResourceBlock: (
    filePath: string,
    key: string,
    newKey: string,
  ) => Promise<boolean>;
  getKeys: (filePath: string, key: string) => Promise<string[] | null>;
};
