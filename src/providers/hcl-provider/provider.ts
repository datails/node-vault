import type { DataSources, UseSandBoxFn } from "../../models";
import {
  delAttrErr,
  createAttrErr,
  updateAttrErr,
  getAttrErr,
  delBlockErr,
  renameBlockErr,
  getKeysErr,
} from "../../errors";

export const createUpdateAttr = (
  dataSources: DataSources,
  useSandBox: UseSandBoxFn,
) => useSandBox(dataSources.wasmClient.updateAttr, updateAttrErr);

export const createCreateAttr = (
  dataSources: DataSources,
  useSandBox: UseSandBoxFn,
) => useSandBox(dataSources.wasmClient.createAttr, createAttrErr);

export const createDelAttr = (
  dataSources: DataSources,
  useSandBox: UseSandBoxFn,
) => useSandBox(dataSources.wasmClient.delAttr, delAttrErr);

export const createDelBlock = (
  dataSources: DataSources,
  useSandBox: UseSandBoxFn,
) => useSandBox(dataSources.wasmClient.delBlock, delBlockErr);

export const createRenameBlock = (
  dataSources: DataSources,
  useSandBox: UseSandBoxFn,
) => useSandBox(dataSources.wasmClient.renameResourceBlock, renameBlockErr);

export const createGetAttr = (
  dataSources: DataSources,
  useSandBox: UseSandBoxFn,
) =>
  useSandBox(dataSources.wasmClient.getAttr, getAttrErr, {
    shouldSyncWebassembly: false,
  });

export const createGetKeys = (
  dataSources: DataSources,
  useSandBox: UseSandBoxFn,
) =>
  useSandBox(dataSources.wasmClient.getKeys, getKeysErr, {
    shouldSyncWebassembly: false,
  });

export type HclProvider = ReturnType<typeof createHclProvider>;

export const createHclProvider = (
  dataSources: DataSources,
  useSandbox: UseSandBoxFn,
) => ({
  updateAttr: createUpdateAttr(dataSources, useSandbox),
  createAttr: createCreateAttr(dataSources, useSandbox),
  delAttr: createDelAttr(dataSources, useSandbox),
  getAttr: createGetAttr(dataSources, useSandbox),
  getKeys: createGetKeys(dataSources, useSandbox),
  delBlock: createDelBlock(dataSources, useSandbox),
  renameResourceBlock: createRenameBlock(dataSources, useSandbox),
});
